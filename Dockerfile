FROM ubuntu:21.10

ENV DEBIAN_FRONTEND=noninteractive
RUN echo 'APT::Get::Assume-Yes "true";' >> /etc/apt/apt.conf && \
    apt-get update && \
    apt-get install gosu zsh git neovim bat ripgrep ncdu curl moreutils && \
    ln -s /usr/bin/batcat /usr/bin/bat && \
    rm -rf /tmp/* /var/lib/apt/lists/* /root/.cache/*

RUN curl https://github.com/dandavison/delta/releases/download/0.11.3/git-delta-musl_0.11.3_amd64.deb -sL -o git-delta.deb && \
    dpkg -i git-delta.deb && \
    rm git-delta.deb

COPY envuser /usr/local/sbin/envuser
RUN chown root /usr/local/sbin/envuser \
    && chmod 700 /usr/local/sbin/envuser

ENV UNAME="developer" \
    UHOME="/home/developer" \
    UID="1000" \
    USHELL="/bin/zsh"
COPY configs /configs
RUN /configs/create-links.sh && cp -r /root/.fzf /configs
RUN /bin/zsh -c "source /configs/zshrc" && cp -r /root/.zcomet /configs

ENTRYPOINT ["envuser"]
CMD ["zsh"]
