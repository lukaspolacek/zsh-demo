# Lukáš' simplified ZSH setup

This repo contains a demo of my simplified ZSH setup that you can copy and modify for your needs.

![screenshot](./screenshot.png)

# Build and run the container

```shell
docker build -t zsh-demo .
docker run -e TERM -e COLORTERM -it --rm zsh-demo
```

Try:

- Ctrl-R to fuzzy-search through history
- Ctrl-T to fuzzy-search for files in the current directory
- Mistype a command and notice it's red
- Mistype a filename and notice it's white instead of violet
- Start typing previous command from history and notice it's automatically completed, press right arrow when satisfied.
- Previous failed command marks the '❯' character red.
- ... and many more things, which you can also customize. More info below.

# I want to setup ZSH the same way

You can copy the provided `zshrc` to your `~/.zshrc` (or just take the parts that interest you). You can also copy the provided `p10k.zsh` to `~/.p10k.zsh` or run `p10k configure` to configure [the statusline](https://github.com/romkatv/powerlevel10k) according to your taste. Don't forget to [install fonts](https://github.com/romkatv/powerlevel10k/blob/master/font.md). Copy-paste ready commands:

```bash
cd $this_repo
cp configs/zshrc ~/.zshrc
cp configs/p10k.zsh ~/.p10k.zsh # alternatively run 'p10k configure'
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && ~/.fzf/install --all
chsh -s /bin/zsh # if you aren't using zsh already
exec zsh
```

Now that you are set, let's go through the installed plugins.

## powerlevel10k

[powerlevel10k](https://github.com/romkatv/powerlevel10k) is a fast, powerful and configurable theme for zsh. I like to use two lines with a divider and have time on display. You can configure it using `p10k configure` according to your taste. Don't forget to [install fonts](https://github.com/romkatv/powerlevel10k/blob/master/font.md).

![Powerlevel10k Configuration Wizard](https://raw.githubusercontent.com/romkatv/powerlevel10k-media/master/configuration-wizard.gif)

## fzf

[fzf](https://github.com/junegunn/fzf) is a general-purpose command-line fuzzy finder.

Try `Ctrl-R` for fuzzy history search, `Ctrl-T` for command-line file search and `Alt-C` for directory search, `**<Tab>` for file search (again).

Fzf can be plugged to almost any line-based tool, but `Ctrl-R` is when I use it the most.

## zsh-autosuggestions

It's best explained through [a video](https://asciinema.org/a/37390).

# Tools to use instead of standard tools

* `bat` (instead of `cat`)
* `rg` (instead of `grep`)
* `nvim` (instead of `vi/vim`)
* `ncdu` (instead of `du`)
* `delta` (instead of `diff`, mostly in `git diff`)
* `vidir` (instead of filemanager)
