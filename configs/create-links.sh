#!/bin/zsh

script_dir=$(dirname $(readlink -f $0))
ln -sf $script_dir/zshrc ~/.zshrc
ln -sf $script_dir/p10k.zsh ~/.p10k.zsh
ln -sf $script_dir/gitconfig ~/.gitconfig
mkdir -p  ~/.config/bat && ln -sf $script_dir/batrc ~/.config/bat/config

if [ ! -d ~/.fzf ]; then
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
fi
~/.fzf/install --all
